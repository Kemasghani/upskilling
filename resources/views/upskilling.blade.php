@extends('layouts.app')

@push('styles')
    {{-- Karena terdapat placeholder, makanya pakai bootstrap versi ini --}}
    {{--    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous"> --}}

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.2/font/bootstrap-icons.min.css">
    <link rel="stylesheet" href="{{ asset('css/upskilling/home.css') }}">
    <link rel="stylesheet" href="{{ asset('css/upskilling/card.css') }}">

    <style>
        .banner-right {
            top: 45%;
        }

        .banner-right .play-button {
            left: 13%;
        }

        .nav-btn-category.bg-light,
        .container-lms-upskilling .btn.btn-light,
        .container-btn-expand {
            background: #F5F5F5 !important;
        }

        .nav-btn-category button {
            width: 140px;
            height: 42px;
            font-size: 12px;
        }

        .nav-btn-category button * {
            margin: 0;
            padding: 0;
        }

        .nav-btn-category button .transbox {
            background-color: rgba(149, 165, 166, 0.1);
            -webkit-backdrop-filter: blur(2px);
            backdrop-filter: blur(2px);
        }

        .nav-btn-category button.active .transbox {
            background-color: rgba(0, 0, 0, 0.5);
        }

        /* Efek transparan di sebelah kiri */
        .btn-category-expand::before {
            content: "";
            position: absolute;
            top: 0;
            left: -200px;
            width: 200px;
            /* Lebar efek transparan */
            height: 100%;
            background: linear-gradient(to right, rgba(245, 245, 245, 0), rgba(245, 245, 245, 1));
        }

        .container-lms-upskilling {
            margin-bottom: 24rem;
        }

        .container-lms-upskilling .blog {
            margin-bottom: 6.25rem;
        }

        .container-lms-upskilling .blog h1 {
            margin-top: 5rem;
        }

        .container-btn-expand {
            padding-top: .3rem;
            padding-bottom: .3rem;
        }
    </style>
@endpush

@section('content')
    <div class="container container-lms-upskilling py-5">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Upskilling</a></li>
                <li class="breadcrumb-item active" aria-current="page">Explore Courses</li>
            </ol>
        </nav>

        {{-- Banner --}}
        <div class="position-relative text-white">
            <img src="{{ asset('images/bg-linear-gradient-blue.png') }}" class="w-100"
                alt="Linear Gradient Background" height="360px">
            <div class="position-absolute top-50 start-0 translate-middle-y p-5">
                <h1 class="mb-4">Access to learning <br> anytime and anywhere</h1>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting <br>
                    industry. Lorem Ipsum has been the industry's standard dummy
                </p>
            </div>
            <div class="position-absolute end-0 translate-middle-y banner-right">
                <img src="{{ asset('images/3d-play-button.png') }}" alt="Girl Model Upskilling"
                    class="position-relative play-button">
                <img src="{{ asset('images/girl-model.png') }}" alt="Girl Model Upskilling">
            </div>

            <div class="position-absolute top-100 start-100 translate-middle">
                <img src="{{ asset('images/cursor.png') }}" alt="Cursor" width="140">
            </div>
        </div>
        {{-- End of Banner --}}

        <div>
            {{-- Category Tabs --}}
            <div class="d-flex align-items-center position-relative">
                <ul class="nav nav-pills nav-btn-category bg-light rounded-start p-2 my-4 gap-2"
                    :class="expanded ? 'w-100' : 'w-75'" id="pills-tab">
                    <li class="nav-item">
                        <button class="nav-link rounded bg-transparent text-white active px-0 position-relative"
                            id="pills-home-tab" data-bs-toggle="pill" type="button" role="tab"
                            aria-controls="pills-home" aria-selected="true"
                            style="background-image: url('/images/bg-design-category.png');">
                            <span
                                class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                Design
                            </span>
                        </button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                            id="pills-profile-tab" data-bs-toggle="pill" type="button" role="tab"
                            aria-controls="pills-profile" aria-selected="false"
                            style="background-image: url('/images/bg-it-tech-category.png');">
                            <span
                                class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                IT & Tech
                            </span>
                        </button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                            id="pills-contact-tab" data-bs-toggle="pill" type="button" role="tab"
                            aria-controls="pills-contact" aria-selected="false"
                            style="background-image: url('/images/bg-finance-category.png');">
                            <span
                                class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                Finance
                            </span>
                        </button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                            id="pills-disabled-tab" data-bs-toggle="pill" type="button" role="tab"
                            aria-controls="pills-disabled" aria-selected="false"
                            style="background-image: url('/images/bg-sales-marketing-category.png');">
                            <span
                                class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                Sales & Marketing
                            </span>
                        </button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                            id="pills-disabled-tab" data-bs-toggle="pill" type="button" role="tab"
                            aria-controls="pills-disabled" aria-selected="false"
                            style="background-image: url('/images/bg-personal-dev-category.png');">
                            <span
                                class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                Personal Development
                            </span>
                        </button>
                    </li>
                    <li class="nav-item">
                        <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                            id="pills-disabled-tab" data-bs-toggle="pill" type="button" role="tab"
                            aria-controls="pills-disabled" aria-selected="false"
                            style="background-image: url('/images/bg-business-category.png');">
                            <span
                                class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                Business
                            </span>
                        </button>
                    </li>

                    <template>
                        <li class="nav-item">
                            <button class="nav-link rounded bg-transparent text-white active px-0 position-relative"
                                id="pills-home-tab" data-bs-toggle="pill" type="button" role="tab"
                                aria-controls="pills-home" aria-selected="true"
                                style="background-image: url('/images/bg-design-category.png');">
                                <span
                                    class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                    Design
                                </span>
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                                id="pills-profile-tab" data-bs-toggle="pill" type="button" role="tab"
                                aria-controls="pills-profile" aria-selected="false"
                                style="background-image: url('/images/bg-it-tech-category.png');">
                                <span
                                    class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                    IT & Tech
                                </span>
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                                id="pills-contact-tab" data-bs-toggle="pill" type="button" role="tab"
                                aria-controls="pills-contact" aria-selected="false"
                                style="background-image: url('/images/bg-finance-category.png');">
                                <span
                                    class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                    Finance
                                </span>
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                                id="pills-disabled-tab" data-bs-toggle="pill" type="button" role="tab"
                                aria-controls="pills-disabled" aria-selected="false"
                                style="background-image: url('/images/bg-sales-marketing-category.png');">
                                <span
                                    class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                    Sales & Marketing
                                </span>
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                                id="pills-disabled-tab" data-bs-toggle="pill" type="button" role="tab"
                                aria-controls="pills-disabled" aria-selected="false"
                                style="background-image: url('/images/bg-personal-dev-category.png');">
                                <span
                                    class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                    Personal Development
                                </span>
                            </button>
                        </li>
                        <li class="nav-item">
                            <button class="nav-link rounded bg-transparent text-white px-0 position-relative"
                                id="pills-disabled-tab" data-bs-toggle="pill" type="button" role="tab"
                                aria-controls="pills-disabled" aria-selected="false"
                                style="background-image: url('/images/bg-business-category.png');">
                                <span
                                    class="transbox rounded position-absolute top-0 start-0 w-100 h-100 d-flex align-items-center justify-content-center">
                                    Business
                                </span>
                            </button>
                        </li>
                    </template>
                </ul>

                <div class="container-btn-expand bg-light rounded-end">
                    <button class="btn btn-light btn-category-expand position-relative border-0 shadow-none">
                        <i @click="expanded = !expanded" class="bi bi-chevron-right fs-4"></i>
                    </button>
                </div>
            </div>
            {{-- End of Category Tabs --}}

            {{-- Button Filter --}}
            <div class="d-flex justify-content-between">
                <div class="d-flex gap-3">
                    <div class="dropdown">
                        <button class="btn btn btn-outline-primary rounded-pill dropdown-toggle" type="button"
                            id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Posted at
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </div>

                    <div class="dropdown">
                        <button class="btn btn btn-outline-primary rounded-pill dropdown-toggle" type="button"
                            id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Category
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </div>

                    <div class="dropdown">
                        <button class="btn btn btn-outline-primary rounded-pill dropdown-toggle" type="button"
                            id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            Company
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="#">Action</a></li>
                            <li><a class="dropdown-item" href="#">Another action</a></li>
                            <li><a class="dropdown-item" href="#">Something else here</a></li>
                        </ul>
                    </div>
                </div>

                <span>Filter by</span>
            </div>
            {{-- End of Button Filter --}}

            <hr>

            {{-- Course Section --}}
            <section class="course-section mt-5">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="d-flex align-items-center">
                        <h2 class="text-primary ms-0 me-4">Creative & Design</h2>
                        <img src="{{ asset('images/shapes.png') }}" alt="Shapes Img" width="60">
                    </div>

                    <div class="d-flex align-items-center gap-3">
                        <a href="#" class="text-secondary text-decoration-underline fw-bold fs-5">Explore
                            Courses</a>
                        <img src="{{ asset('images/arrow.png') }}" alt="Explore Courses Button" class="pt-2">
                    </div>
                </div>

                <div class="row row-cols-1 row-cols-lg-4">
                    <div class="col">
                        <a href="" class="text-decoration-none">
                            <x-card height="150px" />
                        </a>
                    </div>
                </div>
            </section>

            <section class="course-section mt-5">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="d-flex align-items-center">
                        <h2 class="text-primary ms-0 me-4">IT & Tech</h2>
                        <img src="{{ asset('images/komputer.png') }}" alt="Shapes Img" width="60">
                    </div>

                    <div class="d-flex align-items-center gap-3">
                        <a href="#" class="text-secondary text-decoration-underline fw-bold fs-5">Explore
                            Courses</a>
                        <img src="{{ asset('images/arrow.png') }}" alt="Explore Courses Button" class="pt-2">
                    </div>
                </div>

                <div class="row row-cols-1 row-cols-lg-4">

                    <div class="col">
                        <x-card height="150px" />
                    </div>

                </div>
            </section>

            <section class="course-section mt-5">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="d-flex align-items-center">
                        <h2 class="text-primary ms-0 me-4">Sales & Marketing</h2>
                        <img src="{{ asset('images/announce.png') }}" alt="Shapes Img" width="60">
                    </div>

                    <div class="d-flex align-items-center gap-3">
                        <a href="#" class="text-secondary text-decoration-underline fw-bold fs-5">Explore
                            Courses</a>
                        <img src="{{ asset('images/arrow.png') }}" alt="Explore Courses Button" class="pt-2">
                    </div>
                </div>

                <div class="row row-cols-1 row-cols-lg-4">

                    <div class="col">
                        <x-card height="150px" />
                    </div>

                </div>
            </section>

            <section class="course-section mt-5">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="d-flex align-items-center">
                        <h2 class="text-primary ms-0 me-4">Personal Development</h2>
                        <img src="{{ asset('images/rocket.png') }}" alt="Shapes Img" width="60">
                    </div>

                    <div class="d-flex align-items-center gap-3">
                        <a href="#" class="text-secondary text-decoration-underline fw-bold fs-5">Explore
                            Courses</a>
                        <img src="{{ asset('images/arrow.png') }}" alt="Explore Courses Button" class="pt-2">
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-lg-4">
                    <div class="col">
                        <x-card height="150px" />
                    </div>
                </div>
            </section>
            <section class="course-section mt-5">
                <div class="d-flex justify-content-between align-items-center mb-4">
                    <div class="d-flex align-items-center">
                        <h2 class="text-primary ms-0 me-4">Business & Economy</h2>
                        <img src="{{ asset('images/notes.png') }}" alt="Shapes Img" width="60">
                    </div>
                    <div class="d-flex align-items-center gap-3">
                        <a href="#" class="text-secondary text-decoration-underline fw-bold fs-5">Explore
                            Courses</a>
                        <img src="{{ asset('images/arrow.png') }}" alt="Explore Courses Button" class="pt-2">
                    </div>
                </div>
                <div class="row row-cols-1 row-cols-lg-4">
                        <div class="col">
                            <x-card height="150px" />
                        </div>
                </div>
            </section>
            {{-- End of Course Section --}}
            {{-- Blog --}}
            <div class="blog container mt-5">
                <h1 class="text-center fs-1"><span>Blog</span> Lorem ipsum</h1>
                <div class="blogCardContainer d-flex flex-wrap">
                    <div class="blogCard">
                        <img src="{{ asset('images/blockImage.png') }}" alt="blockImage">
                        <h4 class="fs-4 ms-0">Blog 1</h4>
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        </p>
                        <div class="seeAll">See All</div>
                    </div>
                    <div class="blogCard">
                        <img src="{{ asset('images/blockImage.png') }}" alt="blockImage">
                        <h4 class="fs-4 ms-0">Blog 2</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        </p>
                        <div class="seeAll">See All</div>
                    </div>
                    <div class="blogCard">
                        <img src="{{ asset('images/blockImage.png') }}" alt="blockImage">
                        <h4 class="fs-4 ms-0">Blog 3</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        </p>
                        <div class="seeAll">See All</div>
                    </div>
                </div>
            </div>
            {{-- End of Blog --}}
        </div>

        {{-- Testimonial --}}
        <x-review />
        {{-- End of Testimonial --}}
    </div>
@endsection

