<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/blog.css') }}">
</head>

<body>
    @extends('layouts.app')
    @section('content')
        <div class="container containerAll">
            <div class="d-flex containerpageBlog gap-5">
                <div class="leftSectionBlog">
                    <div class="generalImage d-flex flex-column gap-4">
                        <span class="generalTittle">Lorem Ipsum is simply dummy text of the printing</span>
                        <div class="thumbnailContainer">
                            <img src="{{ asset('images/blogThumbnail.png') }}" alt="blogThumbnail" class="thumbnailImage">
                        </div>
                        <div class="infoPostImage d-flex justify-content-center gap-4 gap-md-5">
                            <div class="date d-flex align-items-center gap-1 gap-md-2">
                                <div>
                                    <img src="{{ asset('images/postDate.svg') }}" alt="postDate">
                                </div>
                                <span>November 14 , 2023</span>
                            </div>
                            <div class="comment d-flex align-items-center gap-1 gap-md-2">
                                <div>
                                    <img src="{{ asset('images/postComment.svg') }}" alt="postComment">
                                </div>
                                <span>comments : 35</span>
                            </div>
                            <div class="category d-flex align-items-center gap-1 gap-md-2">
                                <div>
                                    <img src="{{ asset('images/postCategory.svg') }}" alt="postCategory">
                                </div>
                                <span>Category : Design</span>
                            </div>
                        </div>
                    </div>
                    <div class="contentBlog d-flex flex-column gap-3">
                        <span class="tittleDetailBlog">What is Lorem Ipsum?</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                            but also the leap into electronic typesetting, remaining essentially unchanged. It was
                            popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                            and more recently with desktop publishing software like Aldus PageMaker including versions of
                            Lorem Ipsum.</p>
                        <div class="contentImage mt-3 mb-5">
                            <img src="{{ asset('images/contentBlog.png') }}" alt="contentBlog">
                        </div>
                        <span class="tittleDetailBlog">What is Lorem Ipsum?</span>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                            but also the leap into electronic typesetting, remaining essentially unchanged. It was
                            popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                            and more recently with desktop publishing software like Aldus PageMaker including versions of
                            Lorem Ipsum.
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                            but also the leap into electronic typesetting, remaining essentially unchanged. It was
                            popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                            and more recently with desktop publishing software like Aldus PageMaker including versions of
                            Lorem Ipsum.
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                            of type and scrambled it to make a type specimen book. It has survived .</p>
                    </div>
                    <div class="commentSectionContainer">
                        <div class="tittleCommentSection d-flex gap-3 align-items-center mb-3">
                            <div class="mark red"></div>
                            <span>Comments</span>
                        </div>
                        <div class="commentSection">
                            <div class="mainComment replied d-flex flex-column gap-3">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="profileContainer d-flex">
                                        <div class="imageProfile">
                                            <img src="{{ asset('images/profileComment.png') }}" alt="profileComment">
                                        </div>
                                        <div class="textProfile d-flex flex-column justify-content-between py-1 px-2 px-md-3">
                                            <span class="name">Agraditya Putra</span>
                                            <div class="d-flex gap-1 gap-md-2 align-items-center">
                                                <div>
                                                    <img src="{{ asset('images/postDate.svg') }}" alt="postDate">
                                                </div>
                                                <span class="dateComment">2023 04 November</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="replyContainer d-flex justify-content-center align-items-center gap-2">
                                        <div class="replyIcon">
                                            <img src="{{ asset('images/replyIcon.svg') }}" alt="replyIcon">
                                        </div>
                                        <span class="replyText">Reply</span>
                                    </div>
                                </div>
                                <p class="contentComment">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book
                                </p>
                            </div>
                            <div class="replyCommentContainer m-auto d-flex flex-column">
                                <div class="replyComment p-4 bg-white d-flex flex-column gap-3">
                                    <div class="profileContainer d-flex">
                                        <div class="imageProfile">
                                            <img src="{{ asset('images/profileComment.png') }}" alt="profileComment">
                                        </div>
                                        <div class="textProfile d-flex flex-column justify-content-between py-1 px-2 px-md-3">
                                            <span class="name">Agraditya Putra</span>
                                            <div class="d-flex gap-1 gap-md-2 align-items-center">
                                                <div>
                                                    <img src="{{ asset('images/postDate.svg') }}" alt="postDate">
                                                </div>
                                                <span class="dateComment">2023 04 November</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="contentComment">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an unknown
                                        printer took a galley of type and scrambled it to make a type specimen book
                                    </p>
                                </div>
                                <div class="replyComment p-4 bg-white d-flex flex-column gap-3">
                                    <div class="profileContainer d-flex">
                                        <div class="imageProfile">
                                            <img src="{{ asset('images/profileComment.png') }}" alt="profileComment">
                                        </div>
                                        <div class="textProfile d-flex flex-column justify-content-between py-1 px-2 px-md-3">
                                            <span class="name">Agraditya Putra</span>
                                            <div class="d-flex gap-1 gap-md-2 align-items-center">
                                                <div>
                                                    <img src="{{ asset('images/postDate.svg') }}" alt="postDate">
                                                </div>
                                                <span class="dateComment">2023 04 November</span>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="contentComment">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum
                                        has been the industry's standard dummy text ever since the 1500s, when an unknown
                                        printer took a galley of type and scrambled it to make a type specimen book
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="commentSection">
                            <div class="mainComment d-flex flex-column gap-3">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="profileContainer d-flex">
                                        <div class="imageProfile">
                                            <img src="{{ asset('images/profileComment.png') }}" alt="profileComment">
                                        </div>
                                        <div class="textProfile d-flex flex-column justify-content-between py-1 px-2 px-md-3">
                                            <span class="name">Agraditya Putra</span>
                                            <div class="d-flex gap-1 gap-md-2 align-items-center">
                                                <div>
                                                    <img src="{{ asset('images/postDate.svg') }}" alt="postDate">
                                                </div>
                                                <div class="dateComment">2023 04 November</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="replyContainer d-flex justify-content-center align-items-center gap-2">
                                        <div class="replyIcon">
                                            <img src="{{ asset('images/replyIcon.svg') }}" alt="replyIcon">
                                        </div>
                                        <span class="replyText">Reply</span>
                                    </div>
                                </div>
                                <p class="contentComment">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                    has been the industry's standard dummy text ever since the 1500s, when an unknown
                                    printer took a galley of type and scrambled it to make a type specimen book
                                </p>
                            </div>
                        </div>
                        <div class="addComment mt-5">
                            <div class="tittleAddCommentContainer d-flex gap-3 align-items-center">
                                <div class="mark red"></div>
                                <span class="tittleAddComment">Add a comment</span>
                            </div>
                            <div class="inputUserCommentContainer d-flex gap-3 mt-4">
                                <div class="w-100 personalInfo d-flex flex-column gap-3">
                                    <div class="d-flex flex-column gap-2">
                                        <span>Name</span>
                                        <input type="text">
                                    </div>
                                    <div class="d-flex flex-column gap-2">
                                        <span>Website</span>
                                        <input type="text">
                                    </div>
                                    <div class="d-flex flex-column gap-2">
                                        <span>Email</span>
                                        <input type="email">
                                    </div>
                                </div>
                                <div class="w-100 d-flex flex-column gap-2 personalComment">
                                    <span>Comment</span>
                                    <textarea name="" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="actionUser d-flex align-items-center mt-4 gap-1">
                                <div class="w-100 rateUsefull d-flex justify-content-between align-items-center">
                                    <span>Rate the usefulness of the article</span>
                                    <button class="rateGood text-nowrap">
                                        <img src="{{ asset('images/emotGood.svg') }}" alt="emotGood">
                                        <span class="text-light">Good</span>
                                    </button>
                                </div>
                                <button class="send text-light">Send Comment</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rightSectionBlog d-flex flex-column gap-4">
                    <div class="actionBlog d-flex justify-content-between gap-2">
                        <div class="d-flex gap-1 py-1 px-4 px-md-3 action">
                            <div>
                                <img src="{{ asset('images/shareIcon.svg') }}" alt="postComment">
                            </div>
                            <span>share</span>
                        </div>
                        <div class="d-flex gap-1 py-1 px-4 px-md-3 action">
                            <div>
                                <img src="{{ asset('images/saveIcon.svg') }}" alt="saveIcon">
                            </div>
                            <span>marking</span>
                        </div>
                        <div class="d-flex gap-1 py-1 px-4 px-md-3 action">
                            <div>
                                <img src="{{ asset('images/postComment.svg') }}" alt="shareIcon">
                            </div>
                            <span>comment</span>
                        </div>
                    </div>
                    <div class="tagBlogContainer">
                        <div class="tittleTagContainer d-flex align-items-center gap-2 mb-3">
                            <div class="mark"></div>
                            <span>Tags</span>
                        </div>
                        <div class="tagContainer d-flex gap-3">
                            <span class="tag">Design</span>
                            <span class="tag">IT</span>
                            <span class="tag">UI/UX</span>
                            <span class="tag">Frontend</span>
                        </div>
                    </div>
                    <div class="topPostContainer">
                        <div class="tittleTopPostContainer d-flex align-items-center gap-2 mb-3">
                            <div class="mark"></div>
                            <span>Top post</span>
                        </div>
                        <div class="cardTopPostContainer d-flex flex-column gap-4">
                            <div class="cardTopPost d-flex gap-2">
                                <div class="imageTopPost d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('images/otherBlog.png') }}" alt="otherBlog">
                                </div>
                                <div class="textCardTopPost d-flex flex-column justify-content-between py-2">
                                    <span id="tittleTopPost">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry</span>
                                    <p>Subhead</p>
                                </div>
                            </div>
                            <div class="cardTopPost d-flex gap-2">
                                <div class="imageTopPost d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('images/otherBlog.png') }}" alt="otherBlog">
                                </div>
                                <div class="textCardTopPost d-flex flex-column justify-content-between py-2">
                                    <span id="tittleTopPost">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry</span>
                                    <p>Subhead</p>
                                </div>
                            </div>
                            <div class="cardTopPost d-flex gap-2">
                                <div class="imageTopPost d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('images/otherBlog.png') }}" alt="otherBlog">
                                </div>
                                <div class="textCardTopPost d-flex flex-column justify-content-between py-2">
                                    <span id="tittleTopPost">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry</span>
                                    <p>Subhead</p>
                                </div>
                            </div>
                            <div class="cardTopPost d-flex gap-2">
                                <div class="imageTopPost d-flex justify-content-center align-items-center">
                                    <img src="{{ asset('images/otherBlog.png') }}" alt="otherBlog">
                                </div>
                                <div class="textCardTopPost d-flex flex-column justify-content-between py-2">
                                    <span id="tittleTopPost">Lorem Ipsum is simply dummy text of the printing and
                                        typesetting industry</span>
                                    <p>Subhead</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="otherBlog">
                <h1 class="text-center"><span>Blog</span> Lorem ipsum</h1>
                <div class="otherBlogCardContainer d-flex flex-wrap justify-content-center justify-content-lg-between">
                    <x-blogCard />
                    <x-blogCard />
                    <x-blogCard />
                </div>
            </div>
            <div class="banner">
                <img src="{{ asset('images/bannerDivision.png') }}" alt="bannerDivision" class="w-100">
            </div>
        </div>
    @endsection
</body>

</html>
