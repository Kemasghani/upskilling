function setDefaultScroll() {
    var container = document.getElementById("scrollContainer");
    var totalScrollWidth = container.scrollWidth;
    var containerWidth = container.clientWidth;
    var middlePoint = (totalScrollWidth - containerWidth) / 2;

    // Set the default scroll position to the middle
    container.scrollLeft = middlePoint;
}

// Call the function when the page loads
window.onload = setDefaultScroll;

// Function to track the scroll position
function trackScroll() {
    var container = document.getElementById("scrollContainer");
    var cards = document.querySelectorAll(".cardPackage");

    cards.forEach(function (card) {
        var cardCenter = card.offsetLeft + card.offsetWidth / 2;
        var containerCenter = container.scrollLeft + container.clientWidth / 2;

        if (Math.abs(cardCenter - containerCenter) < container.clientWidth / 4) {
            card.classList.add("large");
        } else {
            card.classList.remove("large");
        }
    });
}
