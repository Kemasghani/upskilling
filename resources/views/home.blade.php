<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
</head>

<body>
    @extends('layouts.app')

    @section('content')
        <div class="container">
            <div class="hero d-flex justify-content-between align-items-center">
                <div class="textHero d-flex">
                    <div class="leftTextHero d-flex flex-column gap-2 gap-md-4">
                        <div class="mLogoDashboard">
                            <img src="{{ asset('images/MLogo.png') }}" alt="MLogo" style="width: 100%; height: 100%;">
                        </div>
                        <h1>LOREM IPSUM DOLOR</h1>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum </p>
                        <div class="buttonDashboard d-flex gap-4">
                            <button class="leftButton">Pelajari disini</button>
                            <button class="rightButton">Daftar</button>
                        </div>
                    </div>
                    <div class="heroMobile">
                        <img src="{{ asset('images/heroMobile.png') }}" alt="heroMobile" style="width: 100%; height: 100%;">
                    </div>
                </div>
                <div class="totalStudent">
                    <img src="{{ asset('images/totalStudent.png') }}" alt="totalStudent" style="width: 100%; height: 100%;">
                </div>
                <div class="imageHero">
                    <img src="{{ asset('images/heroImage.png') }}" alt="heroImage" style="width: 100%; height: 100%;">
                </div>
            </div>
            <div class="play3d">
                <img src="{{ asset('images/bluePlayButton3d.png') }}" alt="bluePlayButton3d">
            </div>
            <div class="infoUpskilling d-flex justify-content-between align-items-end">
                <div class="containerPeople">
                    <div class="backgroundPeople"></div>
                    <img src="{{ asset('images/people.png') }}" alt="people" class="people">
                </div>
                <div class="textInfoUpskilling d-flex flex-column gap-3">
                    <h1>Access to learning anytime and anywhere</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy</p>
                    <div class="totalEachSectionContainer d-flex gap-5">
                        <div class="totalSectionLeft d-flex flex-column">
                            <div class="totalEach lessons d-flex flex-column">
                                <span class="number">300+</span>
                                <span class="text">Lessons</span>
                            </div>
                            <div class="totalEach expertLearners d-flex flex-column">
                                <span class="number">20+</span>
                                <span class="text">Expert Learners</span>
                            </div>
                        </div>
                        <div class="totalSectionRight d-flex flex-column">
                            <div class="totalEach successfulStudents d-flex flex-column">
                                <span class="number">300+</span>
                                <span class="text">Successful Students</span>
                            </div>
                            <div class="totalEach students d-flex flex-column">
                                <span class="number">20+</span>
                                <span class="text">Students</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mLogo">
            <img src="{{ asset('images/mGroup.png') }}" alt="mGroup">
        </div>
        <div class="package">
            <h1 class="text-center">Lorem Ipsum dolor sit amet</h1>
            <div class="cardPackageContainer gap-5" id="scrollContainer" onscroll="trackScroll()">
                <div class="cardPackage">
                    <h3 class="tittlePackage">Freemium</h3>
                    <p class="descPackage">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p class="pricePackage">Rp.2.000.000</p>
                    <button>Pilih paket ini</button>
                    <div class="detailPackageContainer d-flex flex-column gap-2">
                        <div class="true d-flex align-items-center gap-3">
                            <div class="iconTrue">
                                <img src="{{ asset('images/true.png') }}" alt="true">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="true d-flex align-items-center gap-3">
                            <div class="iconTrue">
                                <img src="{{ asset('images/true.png') }}" alt="true">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="false d-flex align-items-center gap-3">
                            <div class="iconFalse">
                                <img src="{{ asset('images/false.png') }}" alt="false">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="false d-flex align-items-center gap-3">
                            <div class="iconFalse">
                                <img src="{{ asset('images/false.png') }}" alt="false">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                    </div>
                </div>
                <div class="cardPackage blue">
                    <h3 class="tittlePackage">Freemium</h3>
                    <p class="descPackage">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p class="pricePackage">Rp.2.000.000</p>
                    <button>Pilih paket ini</button>
                    <div class="detailPackageContainer d-flex flex-column gap-2">
                        <div class="true d-flex align-items-center gap-3">
                            <div class="iconTrue">
                                <img src="{{ asset('images/true.png') }}" alt="true">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="true d-flex align-items-center gap-3">
                            <div class="iconTrue">
                                <img src="{{ asset('images/true.png') }}" alt="true">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="false d-flex align-items-center gap-3">
                            <div class="iconFalse">
                                <img src="{{ asset('images/false.png') }}" alt="false">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="false d-flex align-items-center gap-3">
                            <div class="iconFalse">
                                <img src="{{ asset('images/false.png') }}" alt="false">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                    </div>
                </div>
                <div class="cardPackage">
                    <h3 class="tittlePackage">Freemium</h3>
                    <p class="descPackage">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <p class="pricePackage">Rp.2.000.000</p>
                    <button>Pilih paket ini</button>
                    <div class="detailPackageContainer d-flex flex-column gap-2">
                        <div class="true d-flex d-flex align-items-center gap-3">
                            <div class="iconTrue">
                                <img src="{{ asset('images/true.png') }}" alt="true">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="true d-flex d-flex align-items-center gap-3">
                            <div class="iconTrue">
                                <img src="{{ asset('images/true.png') }}" alt="true">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="false d-flex d-flex align-items-center gap-3">
                            <div class="iconFalse">
                                <img src="{{ asset('images/false.png') }}" alt="false">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                        <div class="false d-flex d-flex align-items-center   gap-3">
                            <div class="iconFalse">
                                <img src="{{ asset('images/false.png') }}" alt="false">
                            </div>
                            <div class="textDetailPackage">Lorem Ipsum is simply dummy text</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="saveDiscount d-flex align-items-end justify-content-center">
            <div>
                <img src="{{ asset('images/arrowCurve.png') }}" alt="arrowCurve">
            </div>
            <p>Save 25%</p>
        </div>
        <div class="containerContentDashboard d-flex flex-column">
            <div class="container">
                <div class="d-flex flex-column tittleContainer">
                    <div class="d-flex justify-content-between align-items-baseline tittleTop">
                        <div class="tittle d-flex gap-3 align-items-end">
                            <h3>Kumpulan Program khusus special</h3>
                            <div class="clockImage">
                                <img src="{{ asset('images/clock.png') }}" alt="playButton" style="width: 100%">
                            </div>
                        </div>
                        <div class="d-flex gap-3 exploreContainer">
                            <p class="exploreText">Explore Courses</p>
                            <div class="exploreArrow">
                                <img src="{{ asset('images/arrow.png') }}" alt="playButton" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                    <span class="courseContainerDescription">Program yang paling hits!</span>
                </div>
                <div class="d-flex gap-5 flex-column">
                    <div class="gap-4 highlightContainer">
                        <div class="highlightCard" style="width: 100%;">
                            <x-card containerWidth="100%" containerHeight="100%"
                                tittle="Digital Marketing 101: Sosial Media Marketing" size="796x196" height="196px" />
                        </div>
                        <div class="highlightSecond highlightCard">
                            <x-card containerWidth="100%" containerHeight="100%"
                                tittle="UI/UX Design: Membuat Wireframe di Figma" size="380x196" height="196px" />
                        </div>
                    </div>
                    <div class="highlightSliderContainer">
                        <div id="custom-carousel" class="carousel slide" data-bs-ride="carousel">
                            <!-- Slides -->
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <div class="highlightCard" style="width: 100%;">
                                        <x-card containerWidth="100%" containerHeight="100%"
                                            tittle="Digital Marketing 101: Sosial Media Marketing" size="796x196"
                                            height="196px" />
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="highlightCard" style="width: 100%;">
                                        <x-card containerWidth="100%" containerHeight="100%"
                                            tittle="Digital Marketing 101: Sosial Media Marketing" size="796x196"
                                            height="196px" />
                                    </div>
                                </div>
                                <div class="carousel-item">
                                    <div class="highlightCard" style="width: 100%;">
                                        <x-card containerWidth="100%" containerHeight="100%"
                                            tittle="Digital Marketing 101: Sosial Media Marketing" size="796x196"
                                            height="196px" />
                                    </div>
                                </div>
                            </div>
                            <!-- Left and Right Arrows -->
                            <a class="carousel-control-prev" href="#custom-carousel" role="button"
                                data-bs-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <img src="{{ asset('images/prevArrow.png') }}" alt="arrowSlide">
                            </a>
                            <a class="carousel-control-next" href="#custom-carousel" role="button"
                                data-bs-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <img src="{{ asset('images/nextArrow.png') }}" alt="arrowSlide">
                            </a>
                        </div>
                    </div>
                    <div id="contentCarousel" class="carousel slide" data-bs-ride="carousel">
                        <!-- Slides -->
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="d-flex flex-wrap justify-content-center carouselItemContainer">
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="d-flex flex-wrap justify-content-center carouselItemContainer">
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="d-flex flex-wrap justify-content-center carouselItemContainer">
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                    <div>
                                        <x-cardSmallest tittle="Frontend: Membuat website dasar css" size="284x146" />
                                    </div>
                                </div>
                            </div>
                            <!-- Add more carousel-item entries for additional content slides -->
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#contentCarousel"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <img src="{{ asset('images/prevArrow.png') }}" alt="arrowSlide" class="prevArrow">
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#contentCarousel"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <img src="{{ asset('images/nextArrow.png') }}" alt="arrowSlide" class="nextArrow">
                        </button>
                        <ul class="carousel-indicators">
                            <li data-bs-target="#contentCarousel" data-bs-slide-to="0" class="active"></li>
                            <li data-bs-target="#contentCarousel" data-bs-slide-to="1"></li>
                            <li data-bs-target="#contentCarousel" data-bs-slide-to="2"></li>
                        </ul>

                    </div>
                </div>
            </div>

            <div>
                <div class="container">
                    <div class="d-flex flex-column">
                        <div class="d-flex justify-content-between align-items-baseline tittleContainer">
                            <div class="tittle d-flex gap-3 align-items-end">
                                <h3>belajar dari partner maxy yuk</h3>
                                <div style="width: 78px; height: 78px;" class="komputer">
                                    <img src="{{ asset('images/komputer.png') }}" alt="playButton">
                                </div>
                            </div>
                            <div class="d-flex gap-3 exploreContainer">
                                <p class="exploreText">Explore Courses</p>
                                <div style="width: 19px; height: 19px;">
                                    <img src="{{ asset('images/arrow.png') }}" alt="playButton">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carouselDeksScreen">
                        <div class="d-flex flex-wrap justify-content-between">
                            <div>
                                <x-cardPartner image="{{ asset('images/blogThumbnail.png') }}"
                                    text="Digital Marketing" />
                            </div>
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                        </div>
                    </div>
                    <div class="carouselMobileScreen">
                        <div class="d-flex flex-wrap justify-content-between">
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                            <div>
                                <x-cardPartner image="{{ asset('images/institution.png') }}" text="Digital Marketing" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-5 container">
                    <img src="{{ asset('images/bannerDivision.png') }}" alt="bannerDivision" style="width: 100%">
                </div>
            </div>
            <div class="blog container">
                <h1 class="text-center"><span>Blog</span> Lorem ipsum</h1>
                <div class="blogCardContainer d-flex flex-wrap">
                    <x-blogCard />
                    <x-blogCard />
                    <x-blogCard />
                </div>
            </div>
            <x-review />
            <div class="container joinContainer d-flex align-items-center">
                <div>
                    <img src="{{ asset('images/MLogo.png') }}" alt="MLogo">
                </div>
                <div class="d-flex joinTextContainer justify-content-between">
                    <div class="d-flex flex-column left">
                        <span class="textWhiteBig">Jadilah</span>
                        <span class="textYellow">Mitra Kami</span>
                    </div>
                    <div class="d-flex flex-column right">
                        <span class="textWhiteSmall">Berpartner dengan maxy dan dapatkan talent terbaik untuk perusahaan
                            anda</span>
                        <div>
                            <button class="textBlack">Gabung dengan Maxy!</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    <script src="{{ url('js/home.js') }}"></script>
</body>

</html>
