<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/blogCard.css') }}">
</head>

<body>
    <div id="blogCard">
        <div>
            <img src="{{ asset('images/blogThumbnail.png') }}" alt="blockImage">
        </div>
        <h2>Blog 1</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
            the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
            of type and scrambled it to make a type specimen book. It has survived not only five centuries,
        </p>
        <div class="seeAll">See All</div>
    </div>
</body>

</html>
