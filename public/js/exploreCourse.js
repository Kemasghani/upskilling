document.addEventListener('DOMContentLoaded', function () {
    var buttonDropdown = document.getElementById('dropdownButton');
    var filterContentMobile = document.querySelector('.filterContentMobile');

    buttonDropdown.addEventListener('click', function () {
        if (filterContentMobile.style.height === '0px' || filterContentMobile.style.height === '') {
            filterContentMobile.style.height = '500px';
        } else {
            filterContentMobile.style.height = '0';
        }
    });
});