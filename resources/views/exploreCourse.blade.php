<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/exploreCourse.css') }}">
</head>

<body>
    @extends('layouts.app')
    @section('content')
        <div class="allContentContainer">
            <div class="Job mt-5">
                <div class="container d-flex flex-column">
                    <div class="filterAndJobContainer d-flex gap-4">
                        <div class="filterJobContainerMobile cursor-pointer">
                            <div class="buttonDropdown d-flex align-items-center border justify-content-between"
                                id="dropdownButton">
                                <div class="d-flex gap-2">
                                    <img src="{{ asset('images/filterIcon.png') }}" alt="filterIcon">
                                    <p class="filterText">Filter by</p>
                                </div>
                                <div>
                                    <img src="{{ asset('images/arrowDropdown.png') }}" alt="arrowDropdown">
                                </div>
                            </div>
                            <div class="filterContentMobile d-flex flex-column gap-4 pt-4">
                                <div class="filterDate">
                                    <h3>Design</h3>
                                    <div class="inputFilterContainer mt-3 d-flex flex-column gap-2">
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>UI/UX Design </p>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>Illustration</p>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>Graphic Design</p>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>All</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="filterRate">
                                    <h3>Rate</h3>
                                    <div class="inputFilterContainer mt-3 d-flex flex-column gap-3">
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filterCompany">
                                    <h3>Company</h3>
                                    <select id="mySelect" class="form-select mt-3 form-select-sm"
                                        aria-label=".form-select-sm">
                                        <option selected class="d-none">Choose here</option>
                                        <option value="Verde">Verde</option>
                                        <option value="Gojek">Gojek</option>
                                        <option value="Shopee">Shopee</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex flex-column" id="filterJobContainer">
                            <div class="tittle d-flex gap-3 align-items-center mb-4">
                                <div>
                                    <img src="{{ asset('images/filterIconDekstop.png') }}" alt="filterIconDekstop">
                                </div>
                                <p>Filter by</p>
                            </div>
                            <div class="filterJobContainer d-flex flex-column gap-4">
                                <div class="filterDate">
                                    <h3>Design</h3>
                                    <div class="inputFilterContainer mt-3 d-flex flex-column gap-2">
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>UI/UX Design </p>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>Illustration</p>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>Graphic Design</p>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="radio" name="all" id="all">
                                            <p>All</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="filterRate">
                                    <h3>Rate</h3>
                                    <div class="inputFilterContainer mt-3 d-flex flex-column gap-3">
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                        <div class="d-flex gap-2">
                                            <input type="checkbox" name="all" id="all">
                                            <div class="d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                                <img src="{{ asset('images/emptyStar.png') }}" alt="emptyStar">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="filterCompany">
                                    <h3>Company</h3>
                                    <select id="mySelect" class="form-select mt-3 form-select-sm"
                                        aria-label=".form-select-sm">
                                        <option selected class="d-none">Choose here</option>
                                        <option value="Verde">Verde</option>
                                        <option value="Gojek">Gojek</option>
                                        <option value="Shopee">Shopee</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="cardContainer d-flex gap-5 flex-wrap justify-content-between">
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cardCourse d-flex">
                                <div class="imageCourse">
                                    <img src="{{ asset('images/vidDashboard.png') }}" alt="vidDashboard">
                                </div>
                                <div class="textCourse d-flex flex-column justify-content-between">
                                    <h1>Cara Menjadi Creative Designer</h1>
                                    <div class="d-flex flex-column gap-0">
                                        <div class="d-flex gap-3 gap-md-4 gap-lg-5 mb-2 mb-md-3 align-items-center">
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div
                                                    class="institutionImageCourse d-flex justify-content-center align-items-center">
                                                    <img src="{{ asset('images/institution.png') }}" alt="institution">
                                                </div>
                                                <span class="institutionText">Maxy Academy</span>
                                            </div>
                                            <div class="d-flex align-items-center gap-1 gap-md-2">
                                                <div>
                                                    <img src="{{ asset('images/playPurple.png') }}" alt="playPurple"
                                                        class="playPurple">
                                                </div>
                                                <span class="totalLessonstext">4 Lessons</span>
                                            </div>
                                            <div class="tagCourse d-flex align-items-center">Design</div>
                                        </div>
                                        <p>Kurikulum ini adalah solusi untuk Anda yang ingin memulai belajar digital
                                            marketing. Kurikulum ini dirancang khusus untuk Anda yang ingin mulai memahami
                                            digital marketing....</p>
                                        <div class="starReview d-flex gap-2">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="m-auto" aria-label="Page navigation example" id="paginationCourse">
                        <ul class="pagination d-lg-flex gap-3 d-none">
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                                <a class="page-link" href="#" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    @endsection
    <script src="{{ asset('js/exploreCourse.js') }}"></script>
</body>

</html>
