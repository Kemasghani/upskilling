<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/review.css') }}">
</head>

<body>
    <div class="review container d-flex justify-content-between">
        <div class="peopleImageReview">
            <img src="{{ asset('images/peopleSingle.png') }}" alt="peopleSingle">
        </div>
        <div class="d-flex flex-column gap-5 gap-lg-0 rightSideReview">
            <div class="d-flex align-items-end totalTalent gap-5">
                <div class="d-flex textTotalTalent">
                    <h1><span>5 million</span> talents get jobs via Maxy Academy</h1>
                </div>
                <div class="Mlogo">
                    <img src="{{ asset('images/mLogoBlue.png') }}" alt="mLogoBlue">
                </div>
            </div>
            <div class="cardReviewContainer">
                <div class="d-flex flex-column gap-4">
                    <div class="cardReview d-flex flex-column justify-content-between">
                        <div class="contentReview d-flex flex-column gap-3">
                            <div class="d-flex gap-3">
                                <div class="profileReview d-flex justify-content-between align-items-center">
                                    <img src="{{ asset('images/profileReview.png') }}" alt="profileReview">
                                </div>
                                <div class="d-flex flex-column justify-content-between">
                                    <span class="nameReview">Agraditya Putra</span>
                                    <div class="ratingReview d-flex gap-2">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex verifContainer align-items-start  gap-2">
                                <img src="{{ asset('images/verif.png') }}" alt="verif">
                                <span>Verified Testimonial</span>
                            </div>
                            <div class="textReview">
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the
                                1500s, when an unknown printer took a galley of...
                            </div>
                        </div>
                    </div>
                    <div class="cardReview d-flex flex-column justify-content-between">
                        <div class="contentReview d-flex flex-column gap-3">
                            <div class="d-flex gap-3">
                                <div class="profileReview d-flex justify-content-between align-items-center">
                                    <img src="{{ asset('images/profileReview.png') }}" alt="profileReview">
                                </div>
                                <div class="d-flex flex-column justify-content-between">
                                    <span class="nameReview">Agraditya Putra</span>
                                    <div class="ratingReview d-flex gap-2">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                        <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex verifContainer align-items-start gap-2">
                                <img src="{{ asset('images/verif.png') }}" alt="verif">
                                <span>Verified Testimonial</span>
                            </div>
                            <div class="textReview">
                                Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the
                                1500s, when an unknown printer took a galley of...
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="contentCarouselReview" class="carousel slide " data-bs-ride="carousel">
                <div class="carousel-inner" id="inner">
                    <div class="carousel-item active">
                        <div class="d-flex justify-content-center carouselItemContainer">
                            <div class="cardReview d-flex flex-column justify-content-between">
                                <div class="contentReview d-flex flex-column gap-3">
                                    <div class="d-flex gap-3">
                                        <div class="profileReview d-flex justify-content-between align-items-center">
                                            <img src="{{ asset('images/profileReview.png') }}" alt="profileReview">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between">
                                            <span class="nameReview">Agraditya Putra</span>
                                            <div class="ratingReview d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex verifContainer align-items-start  gap-2">
                                        <img src="{{ asset('images/verif.png') }}" alt="verif">
                                        <span>Verified Testimonial</span>
                                    </div>
                                    <div class="textReview">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the
                                        1500s, when an unknown printer took a galley of...
                                    </div>
                                </div>
                            </div>
                            <div class="cardReview d-flex flex-column justify-content-between">
                                <div class="contentReview d-flex flex-column gap-3">
                                    <div class="d-flex gap-3">
                                        <div class="profileReview d-flex justify-content-between align-items-center">
                                            <img src="{{ asset('images/profileReview.png') }}" alt="profileReview">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between">
                                            <span class="nameReview">Agraditya Putra</span>
                                            <div class="ratingReview d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex verifContainer align-items-start gap-2">
                                        <img src="{{ asset('images/verif.png') }}" alt="verif">
                                        <span>Verified Testimonial</span>
                                    </div>
                                    <div class="textReview">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the
                                        1500s, when an unknown printer took a galley of...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="d-flex justify-content-center carouselItemContainer">
                            <div class="cardReview d-flex flex-column justify-content-between">
                                <div class="contentReview d-flex flex-column gap-3">
                                    <div class="d-flex gap-3">
                                        <div class="profileReview d-flex justify-content-between align-items-center">
                                            <img src="{{ asset('images/profileReview.png') }}" alt="profileReview">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between">
                                            <span class="nameReview">Agraditya Putra</span>
                                            <div class="ratingReview d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex verifContainer align-items-start gap-2">
                                        <img src="{{ asset('images/verif.png') }}" alt="verif">
                                        <span>Verified Testimonial</span>
                                    </div>
                                    <div class="textReview">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the
                                        1500s, when an unknown printer took a galley of...
                                    </div>
                                </div>

                            </div>
                            <div class="cardReview d-flex flex-column justify-content-between">
                                <div class="contentReview d-flex flex-column gap-3">
                                    <div class="d-flex gap-3">
                                        <div class="profileReview d-flex justify-content-between align-items-center">
                                            <img src="{{ asset('images/profileReview.png') }}" alt="profileReview">
                                        </div>
                                        <div class="d-flex flex-column justify-content-between">
                                            <span class="nameReview">Agraditya Putra</span>
                                            <div class="ratingReview d-flex gap-2">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                                <img src="{{ asset('images/fullStar.png') }}" alt="fullStar">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex verifContainer align-items-start gap-2">
                                        <img src="{{ asset('images/verif.png') }}" alt="verif">
                                        <span>Verified Testimonial</span>
                                    </div>
                                    <div class="textReview">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                        Lorem Ipsum has been the industry's standard dummy text ever since the
                                        1500s, when an unknown printer took a galley of...
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#contentCarouselReview"
                    data-bs-slide="prev">
                    <div class="d-flex justify-content-end align-items-center custom-control-button">
                        <img src="{{ asset('images/arrowNext.png') }}" alt="arrowPrev" class="arrowPrev">
                    </div>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#contentCarouselReview"
                    data-bs-slide="next">
                    <div class="d-flex justify-content-start align-items-center custom-control-button">
                        <img src="{{ asset('images/arrowNext.png') }}" alt="arrowNext" class="arrowNext">
                    </div>
                </button>
            </div>
        </div>
    </div>
</body>

</html>
